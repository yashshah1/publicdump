import sys
def create_subnet(cidr):
	ret = []
	str1 = ""
	for i in range(32):
		if i < cidr:
			str1 += "1"
		else:
			str1 += "0"
		if len(str1) == 8:
			ret.append(int(str1, 2))
			str1 = ""
	return ret
def find_network_id(ip, subnet):
	return [ip[i] & subnet[i] for i in range(4)]
def find_broadcast_id(ip, subnet):
	return [ip[i] | (255 - subnet[i]) for i in range(4)]

def hosts_per_subnet(n):
	return (pow(2, 32 - n) - 2)

def number_of_subnets(n):
	return pow(2, n)
def addr_to_bin(addr):
	str1 = ""
	for i in range(4):
		str1 += '{0:08b}'.format(addr[i])
	return str1
def add_bin(bin1, bin2):
	return '{0:032b}'.format(int(bin1, 2) + int(bin2, 2))
def print_addr(addr):
	return ".".join(list(map(str, (map(lambda x: int(x, 2), [addr[i : i + 8] for i in range(0, 32, 8)])))))
def dec_to_bin(num):
	return '{0:032b}'.format(num)
def check_class(ip):
	if(ip[0] >= 0 and ip[0] <= 127):
		return 'A', addr_to_bin([ip[0], 0, 0, 0]), 8
	if(ip[0] >= 128 and ip[0] <= 191):
		return 'B', addr_to_bin([ip[0], ip[1], 0, 0]), 16
	if(ip[0] >= 192 and ip[0] <= 255):
		return 'C', addr_to_bin([ip[0], ip[1], ip[2], 0]), 24
		
def get_diff_index(iterator):
	for i in range(4):
		curr = iterator[i]
		if curr[1:] != curr[:-1]:
			return i
	return -1		

def get_binary_reps(iterable):
	return list(map(lambda x: '{0:08b}'.format(x), iterable))

def get_first_index(iterable):
	altered_iter = list(zip(*iterable))
	
	for i in range(8):
		if altered_iter[i][1:] != altered_iter[i][:-1]:
			return i
	return -1
	
ch = int(input("1. Subnetting\n2. Supernetting\nEnter your choice: "))
if ch == 1:
	ip, cidr = input("Enter IP Address in CIDR format: ").strip().split('/')
	cidr = int(cidr)
	ip = list(map(int, ip.strip().split('.')))
	for i in ip:
		if i > 255:
			print("Invalid IP!")
			exit(0)
	classname, startaddr, classmin = check_class(ip)
	if cidr < classmin or cidr > 30:
		print("Invalid CIDR")
		exit(0)
	subnet = create_subnet(cidr)
	network_id = find_network_id(ip, subnet)
	hosts = hosts_per_subnet(cidr)
	numsubnets = number_of_subnets(cidr - classmin)
	broadcast_id = find_broadcast_id(ip, subnet)
	
	
	print("Class " + classname)
	print()
	print("Network ID to which given IP Address belongs: " + '.'.join(list(map(str,network_id))))
	print("Broadcast ID to which given IP Address belongs: " + '.'.join(list(map(str, broadcast_id))))
	print()
	print("Hosts per subnet: " + str(hosts))
	print("Number of subnets: " + str(numsubnets))
	
	network_id = startaddr
	hosts = dec_to_bin(hosts + 1)
	one = dec_to_bin(1)
	for i in range(numsubnets):
		print("\nSubnet #" + str(i + 1) + ": ")
		print("Network ID: " + print_addr(network_id))
		broadcast_id = add_bin(network_id, hosts)
		print("Broadcast ID: " + print_addr(broadcast_id))
		network_id = add_bin(broadcast_id, one)
else:
	N = int(input("Enter number of IPs: "))
	values = list(set(input().strip() for _ in range(N)))
	if len(values) == 1:
		print(values[0])
		exit(0)
	ips = []
	for i in values:
		x = i.split('.')
		x = list(map(int, x))
		ips.append(x)


	altered_ips = list(zip(*ips))
	diff = get_diff_index(altered_ips)
	binary_diffs = get_binary_reps(altered_ips[diff])

	index_of_first_diff = get_first_index(binary_diffs)
	number_common = 8 * diff + index_of_first_diff


	final_ip = ['', '', '', '']
	full_segments = number_common // 8
	for i in range(full_segments):
		final_ip[i] = ips[0][i]
	partial_common_bits = number_common - full_segments * 8

	start_of_partial = full_segments

	final_ip[start_of_partial] = '{0:08b}'.format(ips[0][full_segments])[:partial_common_bits]
	filler_zeros = '0' * (8 - partial_common_bits)
	final_ip[start_of_partial] += filler_zeros


	for i in range(start_of_partial + 1, 4):
		final_ip[i] = 0
	for i in range(4):
		try:
			final_ip[i] = int(final_ip[i], 2)
		except:
			pass
	print()
	print('.'.join(map(str, final_ip)) + " /" + str(number_common))
