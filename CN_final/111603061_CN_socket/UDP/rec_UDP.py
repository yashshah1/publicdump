from socket import *
import sys
import select
import os

host = "0.0.0.0"
port = 31415
s = socket(AF_INET, SOCK_DGRAM)
s.bind((host, port))
addr = (host, port)
buf = 1024

data, addr = s.recvfrom(buf)
print "Received file: ", data.strip()

if not os.path.exists("./files"):
    os.mkdir("./files")

f = open("./files/"+data.strip(), "wb")


data, addr = s.recvfrom(buf)
try:
	while data:
		f.write(data)
		s.settimeout(2)
		data, addr = s.recvfrom(buf)
except timeout:
	f.close()
	s.close()
	print "Done"

