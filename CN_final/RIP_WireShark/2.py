import time
flag = 0
flag_c2i = 0
class router:
    def __init__(self, router_id, connections, table):
        self.router_id = router_id
        self.connected_to = connections 
        self.routing_table = table
    # table is a list of lists consisting of network id in 1st column
    # hop count in 2nd and next router in 3rd column

    def update_routing_table(self, sender, table):
        temp = self.routing_table
        new_table = []
        row_to_delete = []
        ln1 = [table[i][0] for i in range(len(table))]
        ln2 = [self.routing_table[i][0] for i in range(len(self.routing_table))]
        global flag
        global flag_c2i

        for i in range(len(ln1)):
            if ln1[i] not in ln2:
                row = table[i][:]
                row[1] += 1
                row[2] = sender
                new_table.append(row)
                flag = 1
            elif ln1[i] in ln2:
                index = -1
                row = table[i][:]
                for j in range(len(ln2)):
                    if(ln1[i] == ln2[j]):
                        index = j
                        break
                if(self.routing_table[j][1] > row[1] + 1):
                    self.routing_table[j][1] = row[1] + 1
                    self.routing_table[j][2] = sender 
                    flag = 1
                # For count to infinity simulation
                # Comment out this part when split horizon is to be used 
                elif(row[2] == self.router_id and self.routing_table[j][2] == sender):
                    self.routing_table[j][1] = row[1] + 1
                    if(self.routing_table[j][1] == 16) :
                        flag_c2i = 1
                    self.routing_table[j][2] = sender
                    flag = 1
                # For Split horizon with route poisoning
                # For split horizon we do not consider updates where next node is the receiver node itself
                '''elif((row[2] == self.router_id or row[2] == -1) and (self.routing_table[j][2] == sender or self.routing_table[j][2] == -1)):
                    if(self.routing_table[j][1] == 16):
                        table[i][1] = 16
                        flag = 1
                    elif(row[1] == 16) :
                        self.routing_table[j][1] = 16
                        flag = 1'''

        for row in new_table:
            if row not in self.routing_table:
                self.routing_table.append(row)
                
    
    def print_table(self):
        print("----------------------------------------------")
        print("|                     {}                      |".format(self.router_id))
        print("----------------------------------------------")
        print("|   Network ID        | Hop Count | Next Hop |")
        for i in range(len(self.routing_table)):
            print("----------------------+-----------+-----------")
            print("|                     |           |          |")
            print("|   {}       |     {}     |    {}    |".format(self.routing_table[i][0], self.routing_table[i][1], self.routing_table[i][2]))
            print("|                     |           |          |")
        print("----------------------------------------------")

def send_updates(router_list, n) :
    # for k in range(16):
    global flag
    global flag_c2i
    flag = 0
    for i in range(1, n + 1):
        print("Router {} sending updates...".format(i))
        for j in range(len(router_list[i - 1].connected_to)):
            # print("{} {} {} {}".format(len(router_list) ,i, j, len(router_list[i - 1].connected_to)))
            router_list[router_list[i - 1].connected_to[j] - 1].update_routing_table(i, router_list[i - 1].routing_table)
    if flag == 0:
        print("\nNetwork converged")
        for i in range(1, n + 1):
            router_list[i - 1].print_table()
            print("\n")
    if flag_c2i == 1:
        print("\nCount to infinity problem occured due to down link")
    for i in range(1, n + 1):
        router_list[i - 1].print_table()
        print("\n")
        
def send_triggered_updates(router_list, i) :
    # for k in range(16):
    global flag
    global flag_c2i
    flag = 0
    print("Router {} sending triggered updates...".format(i))
    for j in range(len(router_list[i - 1].connected_to)):
        # print("{} {} {} {}".format(len(router_list) ,i, j, len(router_list[i - 1].connected_to)))
        router_list[router_list[i - 1].connected_to[j] - 1].update_routing_table(i, router_list[i - 1].routing_table)
    if flag == 0:
        print("\nNetwork converged")
        for i in range(1, len(router_list) + 1):
            router_list[i - 1].print_table()
            print("\n")
    if flag_c2i == 1:
        print("\nCount to infinity problem occured due to down link")
    for i in range(1, len(router_list) + 1):
        router_list[i - 1].print_table()
        print("\n")

if __name__ == '__main__' :
    n = int(input("Enter the number of nodes : "))
    router_list = []

    for i in range(1, n + 1):
        print("Router {}".format(i))
        connected_to = list(map(int, input("Enter the list of directly connected routers : ").split()))
        m = int(input("Enter the number of directly connected networks : "))
        table = []
        for j in range(m):
            network_id = input("Enter the network id of Network {} : ".format(j + 1))
            temp = [network_id, 0, -1]
            table.append(temp)
        temp_router = router(i, connected_to, table)
        print("Router {} table".format(i))
        temp_router.print_table()
        router_list.append(temp_router)
    
    while(True) : 
        send_updates(router_list, n)
        if(flag == 0) :
            break
        time.sleep(10)

    decider = input("Are all links working : ") 
    if(decider == 'n'):
        down_link = int(input("Enter Router id of router whose link is down : "))
        down_network = input("Enter network id of disconnected network : ")
        temp_r = router_list[down_link - 1]    
        for row in temp_r.routing_table :
            if(row[0] == down_network) : 
                row[1] = 16 # Route Poisoning
                row[2] = -1
        print(router_list[down_link - 1].routing_table)
        send_triggered_updates(router_list, down_link - 1)

    while(True) : 
        send_updates(router_list, n)
        if(flag == 0) :
            break
        time.sleep(10)
