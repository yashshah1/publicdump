#include <stdlib.h>
#include <string.h>
#include "queue.h"
void qinit(queue *q) {
	q->head = q->tail = NULL;
}

int qempty(queue *q) {
	return q->head == NULL;
}

int qfull(queue *q) {
	return 0;
}

void enq(queue *q, data d) {
	node *add = (node *) malloc(sizeof(node));

	strcpy((add->d).name, d.name);
	(add->d).age = d.age;
	add->next = q->head;

	if(!(q->head)) {
		q->head = add;
		add->next = add;
	}
	else
		q->tail->next = add;
	q->tail = add;
}

data deq(queue *q) {
	node *temp2 = q->head;
	if(q->head == q->tail)
		q->head = q->tail = NULL;
	else {
		q->head = q->head->next;
		q->tail->next = q->head;
	}
	return temp2->d;
}
