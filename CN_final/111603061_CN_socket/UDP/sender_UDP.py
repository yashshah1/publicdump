from socket import *
import sys
from time import sleep
if len(sys.argv) != 3:
	print "Usage: python sender_UDP.py host filename"
else:
	s = socket(AF_INET, SOCK_DGRAM)
	host = sys.argv[1]
	port = 31415
	buf = 1024
	addr = (host, port)
	
	file_name = sys.argv[2]
	print "Sending ..."
	sleep(1)
	
	s.sendto(file_name, addr)
	f = open(file_name, "rb")
	data = f.read(buf)
        print "sending ..."
	while data:
		if(s.sendto(data, addr)):
			data = f.read(buf)
	s.close()
	f.close()
