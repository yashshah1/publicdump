from __future__ import print_function
import time
import math
import random
import sys
def print_packets(window_size, packet_number, total_packets):
	iterator = 0
	while(iterator < window_size):
		print(str(packet_number), end = " ")
		packet_number += 1
		if packet_number > total_packets:
			break
		iterator += 1
	print()
	return packet_number
option = int(raw_input("Enter the option:\n1. Stop And Wait\n2. Go Back N\n3. Selective Repeat\n4. Taho Algorithm\n5. Reno Algorithm\n"))
if option == 1:
	total_packets = int(raw_input("Enter the number of packets: "))
	lost = raw_input("Enter the list of lost packets in ascending order: ").split(" ")
	if not lost == [""]:
		lost_packets = list(map(int, lost))
	packet_number = 1
	iterator = 0
	end_of_lost = 0
	if len(lost_packets) == 0:
		end_of_lost = 1
	while packet_number <= total_packets:
		if end_of_lost == 0 and packet_number == lost_packets[iterator]:
			print(str(packet_number), end = "\t")
			print("PACKET LOSS")
			iterator += 1
			if iterator == len(lost_packets):
				end_of_lost = 1
		print(str(packet_number), end = "\t")
		packet_number += 1
		print("ACK" + str(packet_number))
elif option == 2:
	total_packets = int(raw_input("Enter the number of packets: "))
	lost = raw_input("Enter the list of lost packets in ascending order: ").split(" ")
	
	if not lost == [""]:
		lost_packets = list(map(int, lost))
	window_size = int(raw_input("Enter window size: "))
	packet_number = 1
	iterator = 0
	end_of_lost = 0
	if len(lost_packets) == 0:
		end_of_lost = 1
	while packet_number <= total_packets:
		packet_number = print_packets(window_size, packet_number, total_packets)
		if end_of_lost == 0 and packet_number > lost_packets[iterator]:
			packet_number = lost_packets[iterator]
			iterator += 1
			if iterator == len(lost_packets):
				end_of_lost = 1
elif option == 3:
	total_packets = int(raw_input("Enter the number of packets: "))
	lost = raw_input("Enter the list of lost packets in ascending order: ").split(" ")
	if not lost == [""]:
		lost_packets = list(map(int, lost))
	window_size = int(raw_input("Enter window size: "))
	packet_number = 1
	iterator = 0
	end_of_lost = 0
	if len(lost_packets) == 0:
		end_of_lost = 1
	while packet_number <= total_packets:
		packet_number = print_packets(window_size, packet_number, total_packets)
		if end_of_lost == 0 and packet_number > lost_packets[iterator]:
			while packet_number > lost_packets[iterator]:
				print(str(lost_packets[iterator]))
				iterator += 1
				if iterator == len(lost_packets):
					end_of_lost = 1
					break
elif option == 4:
	lost_packets = []
	total_packets = int(raw_input("Enter the number of packets: "))
	threshold = int(raw_input("Enter initial threshold: "))
	lost = raw_input("Enter the list of lost packets in ascending order: ").strip().split(" ")
	
	if not lost == [""]:
		lost_packets = list(map(int, lost))
	window_size = 1
	packet_number = 1
	lost_index = 0
	end_of_lost = 0
	if len(lost_packets) == 0:
		end_of_lost = 1
	while packet_number <= total_packets:
		while packet_number <= total_packets:
			if window_size > threshold:
				window_size /= 2
				break
			packet_number = print_packets(window_size, packet_number, total_packets)
			if end_of_lost == 0 and packet_number > lost_packets[lost_index]:
				curr = lost_packets[lost_index]
				while packet_number > lost_packets[lost_index]:
					lost_index += 1
					if lost_index == len(lost_packets):
						lost_index -= 1
						end_of_lost = 1
						break
				packet_number = curr
				threshold = window_size // 2
				window_size = 1
				continue
			window_size *= 2
		while packet_number <= total_packets:
			window_size += 1
			packet_number = print_packets(window_size, packet_number, total_packets)
			if end_of_lost == 0 and packet_number > lost_packets[lost_index]:
				curr = lost_packets[lost_index]
				while packet_number > lost_packets[lost_index]:
					lost_index += 1
					if lost_index == len(lost_packets):
						lost_index -= 1
						end_of_lost = 1
						break
				packet_number = curr
				threshold = window_size // 2
				window_size = 1
				break
elif option == 5:
	lost_packets = []
	total_packets = int(raw_input("Enter the number of packets: "))
	threshold = int(raw_input("Enter initial threshold: "))
	lost = raw_input("Enter the list of lost packets in ascending order: ").split(" ")
	if not lost == [""]:
		lost_packets = list(map(int, lost))
	three_dup = random.randint(1, int(math.log(total_packets, 2)))
	dup3flag = 0
	counter = 1
	window_size = 1
	packet_number = 1
	lost_index = 0
	end_of_lost = 0
	if len(lost_packets) == 0:
		end_of_lost = 1
	while packet_number <= total_packets:
		while packet_number <= total_packets:
			if window_size > threshold:
				window_size /= 2
				print("Going into additive increase...")
				break
			packet_number = print_packets(window_size, packet_number, total_packets)
			counter += 1
			if end_of_lost == 0 and packet_number > lost_packets[lost_index]:
				curr = lost_packets[lost_index]
				while packet_number > lost_packets[lost_index]:
					lost_index += 1
					if lost_index == len(lost_packets):
						lost_index -= 1
						end_of_lost = 1
						break
				packet_number = curr
				threshold = window_size // 2
				window_size = 1
				continue
			if packet_number <= total_packets:
				if counter == three_dup:
					dup3flag = 1
				if dup3flag == 1 and window_size > 4:
					print("*3DUP ACK*")
					threshold = int(0.5 * window_size)
					window_size = int(0.75 * window_size) - 1
					dup3flag = 0
					break
			window_size *= 2
		while packet_number <= total_packets:
			window_size += 1
			packet_number = print_packets(window_size, packet_number, total_packets)
			if end_of_lost == 0 and packet_number > lost_packets[lost_index]:
				curr = lost_packets[lost_index]
				while packet_number > lost_packets[lost_index]:
					lost_index += 1
					if lost_index == len(lost_packets):
						lost_index -= 1
						end_of_lost = 1
						break
				packet_number = curr
				threshold = window_size // 2
				window_size = 1
				break
			if dup3flag == 1 and window_size > 4:
					print("*3DUP ACK*")
					threshold = int(0.5 * window_size)
					window_size = int(0.75 * window_size)
					dup3flag = 0
