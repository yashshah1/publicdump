from math import ceil
from sys import exit
tot_len_data = int(input("Enter total length: ")) - 20
mtu_data = int(input("Enter MTU length: ")) - 20

if tot_len_data < 20:
	print("Minimum data length has to be 20\n")
	exit(-1)


if mtu_data < tot_len_data:
	print("Fragment\tStart\t\tSegment\t\tOffset")
	n = int(ceil(tot_len_data / float(mtu_data)))
	start = 0
	end = start + mtu_data - 1
	for i in range(1, n + 1):
		print(str(i) + "\t\t" + str(start) + "\t\t" + str(start) + " - " + str(end) + "\t" + str(start // 8))
		start = start + mtu_data
		if (start + mtu_data - 1) < tot_len_data:
			end = start + mtu_data - 1
		else:
			end = tot_len_data - 1
else:
	print("No fragmentation required.")
