ch = int(input("1. IP to Decimal\n2. Decimal to IP\nEnter your choice: "))
if ch == 1:
	IP = map(int, input("\nPlease input IP: ").strip().split('.'))

	num = 0
	for i in IP:
		num = (num << 8) + i

	print("IP in base 10: " + str(num)) 

else:
	num = int(input("\nEnter Decimal: "), 10)
	
	IP_addr = [(num >> 24) & 0xFF, (num >> 16) & 0xFF, (num >> 8) & 0xFF, num & 0xFF]
	IP = list(map(str, IP_addr))
	print("IP is " + '.'.join(IP))
