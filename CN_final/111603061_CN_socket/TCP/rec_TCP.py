from __future__ import print_function
import socket
import os

TCP_IP = '127.0.0.1'
TCP_PORT = 31415
BUFFER_SIZE = 32  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
data = conn.recv(32)
if not os.path.exists("./files"):
    os.mkdir("./files")

f = open("./files/" + data + "_recv", "w")
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data:
        break
    f.write(data)
    #print(data, end='',)

f.close()
conn.close()
