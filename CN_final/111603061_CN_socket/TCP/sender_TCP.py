from socket import *
import sys
from time import sleep

if len(sys.argv) != 3:
	print "Usage: python sender_TCP.py host filename"
        
else:
        TCP_IP = sys.argv[1]
        TCP_PORT = 31415
        BUFFER_SIZE = 32

	file_name = sys.argv[2]
	print "Sending ..."
	sleep(2)

        s = socket(AF_INET, SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(file_name)
        sleep(1) 
	f = open(file_name, "rb")
	data = f.read(BUFFER_SIZE)
	while data:
		if(s.send(data)):
			data = f.read(BUFFER_SIZE)


	s.close()
	f.close()
